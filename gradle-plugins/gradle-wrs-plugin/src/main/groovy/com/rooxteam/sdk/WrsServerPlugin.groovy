package com.rooxteam.sdk

import org.gradle.api.GradleException
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.tomcat.tasks.TomcatRunWar
import org.gradle.api.plugins.tomcat.tasks.TomcatStop
import org.gradle.api.tasks.bundling.Jar

/**
 * Created by Konstantin on 19.02.2015.
 */
class WrsServerPlugin implements Plugin<Project> {

    @Override
    void apply(Project target) {
        configureProject(target)
    }

    void configureProject(Project project) {
        project.allprojects {
            apply plugin: 'tomcat-base'

            ext {
                wrsServerVersion = '4.1.3-+'
                tomcatVersion = '7.0.11'
            }
            configurations {
                wrs
            }
            repositories {
                maven {
                    url = 'http://repo.rooxteam.com/customers/content/repositories/roox.releases'
                    if (project.hasProperty('rooxUser') && project.hasProperty('rooxPassword')) {
                        credentials {
                            username project.ext.rooxUser
                            password project.ext.rooxPassword
                        }
                    }
                }
            }
            dependencies {
                tomcat "org.apache.tomcat.embed:tomcat-embed-core:${tomcatVersion}",
                        "org.apache.tomcat.embed:tomcat-embed-logging-juli:${tomcatVersion}"
                tomcat("org.apache.tomcat.embed:tomcat-embed-jasper:${tomcatVersion}") {
                    exclude group: 'org.eclipse.jdt.core.compiler', module: 'ecj'
                }
                wrs("com.rooxteam.wrs:wrs-server-common-pub:${wrsServerVersion}:war@war")
            }
            task([type: Jar, description: '[Internal] Package env-config'], 'package-env-config') {
                from(file('src/main/env-config'));
                archiveName = 'env-config.jar'
            }


            def deps = configurations.wrs?.files
            if (deps.isEmpty()) {
                throw new GradleException("WRS version is not configured")
            }

            def additionalClasspathes = [file('src/main/env-config')]
//            def additionalClasspathes = tasks['package-env-config'].outputs.files.files
            // tasks['package-env-config'].outputs.files.files

            File war = deps.iterator().next()
            task([type: TomcatRunWar, description: 'Start WRS for local development', dependsOn: ['package-env-config']], 'run') {
                contextPath = 'wrs'
                httpPort = "8080" as Integer
                httpsPort = "8084" as Integer
                stopPort = "8081" as Integer
                webApp = war
                additionalRuntimeJars.addAll(additionalClasspathes)
                doFirst {
                    print "Setting dev environment"
                    System.setProperty("RX_ENV", "dev")
                }
            }

            task([type: TomcatRunWar, description: 'Start WRS in backgound for local development', dependsOn: ['package-env-config']], 'start') {
                daemon = true
                contextPath = 'wrs'
                httpPort = "8080" as Integer
                httpsPort = "8084" as Integer
                stopPort = "8081" as Integer
                webApp = war
                additionalRuntimeJars.addAll(additionalClasspathes)
                doFirst { System.setProperty("RX_ENV", "dev") }
            }

            task([type: TomcatStop, description: 'Stop WRS started with task \'start\''], 'stop') {
                stopPort = "8081" as Integer
            }
        }
    }
}
