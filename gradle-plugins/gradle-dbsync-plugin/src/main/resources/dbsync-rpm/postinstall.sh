#!/bin/sh
echo "running post installation script"
service='roox-%appName%'
targetdir='/opt/'$service
username='tomcat'
groupname='tomcat'

set -x

mkdir -p /var/log/$service
chown $username:$groupname /var/log/$service
if [ ! -L $targetdir/logs ];
then
  ln -s /var/log/$service $targetdir/logs
  chown -R $username:$groupname $targetdir/logs
fi
