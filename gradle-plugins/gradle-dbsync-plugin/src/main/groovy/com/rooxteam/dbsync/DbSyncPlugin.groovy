package com.rooxteam.dbsync

import com.rooxteam.corporate.RooxRpmPlugin
import org.apache.tools.ant.filters.ReplaceTokens
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.file.DuplicatesStrategy
import org.gradle.api.tasks.Copy

/**
 * @author Dmitry Tikhonov
 */
class DbSyncPlugin implements Plugin<Project> {

    @Override
    void apply(Project target) {
        configureProject(target)
    }

    void configureProject(Project project) {
        project.allprojects {
            apply plugin: 'roox-jar'
            apply plugin: 'roox-rpm'
            apply plugin: 'base'
            apply plugin: 'application'

            dependencies {
                compile(group: 'com.rooxteam.platform', name: 'db-sync-cli-util', version: '1.0.0-+')
                compile(group: 'com.oracle', name: 'ojdbc6', version: '11.2.0.2.0')
                compile(group: 'org.postgresql', name: 'postgresql', version: '9.3-1102-jdbc41')
            }

            jar {
                manifest {
                    attributes("Implementation-Title": "Gradle",
                            "Implementation-Version": project.version.getWithoutBuild())
                }
            }

            def customTokens = [
                    'appName': project.name
            ]

            mainClassName = "org.springframework.shell.Bootstrap"
            applicationDefaultJvmArgs = ["-Dlogback.configurationFile=/opt/roox-$customTokens.appName/conf/logback.xml"]

            run {
                standardInput = System.in
            }

            rpmPlugin {
                includeWar = false
                includeLogConfig = false
                tokens = customTokens
            }

            task([type: DbSyncRpm, dependsOn: ['jar', 'installApp']], 'makeRpm') {
                distribution = "roox-$project.name"

                requires('java-1.7.0-openjdk-devel')

                from("build/install/$project.name/lib") {
                    into "/opt/roox-$project.name/lib"
                    fileMode = 0444
                    createDirectoryEntry = true
                    user = 'tomcat'
                    permissionGroup = 'tomcat'
                }

                from("build/install/$project.name/bin") {
                    into "/opt/roox-$project.name/bin"
                    fileMode = 0755
                    createDirectoryEntry = true
                    user = 'tomcat'
                    permissionGroup = 'tomcat'
                }

                from("build/conf/logback.xml") {
                    into "/opt/roox-$project.name/conf"
                    fileMode = 0644
                    createDirectoryEntry = true
                    user = 'tomcat'
                    permissionGroup = 'tomcat'
                }

                from("build/conf/dbsync_as_tomcat.sh") {
                    into "/opt/roox-$project.name/bin"
                    fileMode = 0755
                    createDirectoryEntry = true
                    user = 'tomcat'
                    permissionGroup = 'tomcat'
                }
            }
        }
    }

    public static class DbSyncRpm extends RooxRpmPlugin.RooxRpm {
        @Override
        protected void applyConventions() {
            super.applyConventions()

            def pluginJar = getClass().getClassLoader().getResource("dbsync-rpm").getFile().replaceAll('(.+?)!.*', '$1')
            project.task('extractDbSyncResources', type: Copy) {
                from(project.zipTree(pluginJar))
                include('dbsync-rpm/*')
                into('build/confTemplate')
            }

            def substitutionMap = [:]
            substitutionMap.putAll(project.rpmPlugin.tokens)
            project.task('processDbSyncResources', type: Copy, dependsOn: 'extractDbSyncResources') {
                filter(ReplaceTokens, beginToken: '%', endToken: '%', tokens: substitutionMap);
                from('build/confTemplate/dbsync-rpm')
                into('build/conf')
                duplicatesStrategy = DuplicatesStrategy.EXCLUDE
            }.doLast({
                addRpmScripts('build/conf')
            })
            dependsOn('processDbSyncResources')
        }
    }
}
