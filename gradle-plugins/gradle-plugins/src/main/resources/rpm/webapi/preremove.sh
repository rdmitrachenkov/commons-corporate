echo "running pre uninstallation script"

# script variables
service='%serviceName%'

if [ "$1" = 0 ] ; then
    service $service stop
    chkconfig --del $service
fi
