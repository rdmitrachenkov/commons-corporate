package com.rooxteam.corporate

import org.apache.fop.apps.MimeConstants
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.OutputDirectory

/**
 * Генерирует документацию из файлов в формате asciidoc, находящихся в папке src/doc, в стилизации RooX Solutions
 * @author kkorsakov
 */
public class ADocPlugin implements Plugin<Project> {

    private static Logger logger = Logging.getLogger(ADocPlugin);

    @Override
    public void apply(Project project) {
        project.apply(plugin: 'base');
        project.extensions.create("adoc", ADocExtension, project.buildDir.getAbsolutePath());
        project.task('adoc', group: 'Documentation') {
            dependsOn('adoc-html')
            dependsOn('adoc-docbook')
            dependsOn('adoc-pdf')
        };
        project.task('adoc-html', type: ADocTask, group: 'Documentation') {
            format = 'html'
        }
        project.task('adoc-docbook', type: ADocTask, group: 'Documentation') {
            format = 'docbook'
        }
        project.task('adoc-pdf', type: ADocFOPTask, group: 'Documentation') {
            format = 'docbook'
        }
        project.task('adoc-rtf', type: ADocFOPTask, group: 'Documentation') {
            format = 'docbook';
            targetMime = MimeConstants.MIME_RTF;
            extension = "rtf";
        }
    }

    public static class ADocExtension {
        @InputDirectory
        def sourceDir = 'src/doc'

        @OutputDirectory
        def targetDir;

        public ADocExtension() {
            targetDir = "build/doc";
        }

        public ADocExtension(String buildDir) {
            targetDir = "$buildDir/doc";
        }
    }


}

