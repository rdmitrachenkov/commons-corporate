package com.rooxteam.corporate

/**
 * TCP listeners
 */
public enum Listener {

    /**
     * App server shutdown port (used mainly for tomcat)
     */
    SHUTDOWN(0),

    /**
     * SNMP monitoring
     */
    SNMP(1),

    /**
     * Java Management Extensions
     */
    JMX(3),

    /**
     * HTTPS port for general service (if terminated on application server)
     */
    HTTPS(4),

    /**
     * HTTP port for general service
     */
    HTTP(8),

    /**
     * HTTP port for monitoring and management services like JavaMelody, Metrics, etc
     */
    ADMIN(7),

    /**
     * Java remote debug port
     */
    JDWP(9);

    Listener(int id){
        this.id=id;
    }

    int id;

}