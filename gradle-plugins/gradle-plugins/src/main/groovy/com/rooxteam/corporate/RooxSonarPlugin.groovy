package com.rooxteam.corporate

import org.gradle.api.Plugin
import org.gradle.api.Project

class RooxSonarPlugin implements Plugin<Project> {
    void apply(Project project) {
        project.allprojects {
            project.apply(plugin: 'sonar-runner');
            project.apply(plugin: 'jacoco');

            sonarRunner.sonarProperties {
                property("sonar.projectName", project.name)
                property("sonar.projectKey", "$project.group:$project.name")
                property("sonar.binaries", "build/classes")
                property("sonar.jacoco.reportPath", "${project.buildDir}/jacoco/test.exec")
                property("sonar.host.url", "http://jenkins.rooxintra.net:9000")
                property("sonar.jdbc.driverClassName", "com.mysql.jdbc.Driver")
                property("sonar.jdbc.url", "jdbc:mysql://jenkins.rooxintra.net:3306/sonar?useUnicode=true&characterEncoding=utf8&rewriteBatchedStatements=true")
                property("sonar.jdbc.username", "sonar")
                property("sonar.jdbc.password", "sonar")
            }
        }
    }
}
