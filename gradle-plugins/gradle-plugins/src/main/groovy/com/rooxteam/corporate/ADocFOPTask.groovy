package com.rooxteam.corporate

import com.icl.saxon.TransformerFactoryImpl
import org.apache.fop.apps.Fop
import org.apache.fop.apps.FopFactory
import org.apache.fop.apps.MimeConstants
import org.apache.xerces.jaxp.SAXParserFactoryImpl
import org.apache.xml.resolver.CatalogManager
import org.apache.xml.resolver.tools.CatalogResolver
import org.xml.sax.InputSource
import org.xml.sax.XMLReader

import javax.xml.parsers.SAXParserFactory
import javax.xml.transform.Result
import javax.xml.transform.Source
import javax.xml.transform.Transformer
import javax.xml.transform.TransformerFactory
import javax.xml.transform.sax.SAXResult
import javax.xml.transform.sax.SAXSource
import javax.xml.transform.stream.StreamResult
import javax.xml.transform.stream.StreamSource

/**
 * Generate PDF from asciidoc using Apache FOP.
 * File is transformed from asciidoc to docbook, then to FO, then to PDF.
 * May support other FO formats, but not tested specially.
 * Class is tuned for PDF creation by default, but can be customized using field variables.
 * Created by kkorsakov on 17.06.2014.
 */
class ADocFOPTask extends ADocTask {

    /**
     * Stylesheet filename used in transformations. But reside in 'xsl' folder on document's dir
     */
    def stylesheet = "pdf.xsl";

    /**
     * Target mime-type for output document
     */
    String targetMime = MimeConstants.MIME_PDF;

    String extension = "pdf";

    @Override
    protected void postProcessFile(File srcFile) {
        File sourceDir = srcFile.getParentFile();

        SAXParserFactory factory = new SAXParserFactoryImpl();
        factory.setXIncludeAware(true);

        String outputFilename = srcFile.getName().substring(0, srcFile.getName().lastIndexOf(".")) + '.fo';

        // use the same dir
        File oDir = sourceDir;
        File outputFile = new File(oDir, outputFilename);

        Result result = new StreamResult(outputFile.getAbsolutePath());
        CatalogResolver resolver = new CatalogResolver(createCatalogManager());
        InputSource inputSource = new InputSource(srcFile.getAbsolutePath());

        XMLReader reader = factory.newSAXParser().getXMLReader();
        reader.setEntityResolver(resolver);
        TransformerFactory transformerFactory = new TransformerFactoryImpl();
        transformerFactory.setURIResolver(resolver);



        def stylesheetFile = new File("$targetDir/xsl/${stylesheet}");

        URL url = stylesheetFile.toURI().toURL();
        Source source = new StreamSource(url.openStream(), url.toExternalForm());
        Transformer transformer = transformerFactory.newTransformer(source);

        transformer.setParameter("callout.graphics.extension", ".png");
        transformer.setParameter("highlight.source", "1");
        transformer.setParameter("highlight.xslthl.config", new File("$targetDir/highlighting", "xslthl-config.xml").toURI().toURL());

        preFOPTransform(transformer, srcFile, outputFile);

        transformer.transform(new SAXSource(reader, inputSource), result);

        postFOPTransform(outputFile);
    }

    // for some reason, statically typing the return value leads to the following
    // error when Gradle tries to subclass the task class at runtime:
    // java.lang.NoClassDefFoundError: org/apache/xml/resolver/CatalogManager
    private Object createCatalogManager() {
        CatalogManager manager = new CatalogManager();
        manager.setIgnoreMissingProperties(true);
        ClassLoader classLoader = this.getClass().getClassLoader();
        StringBuilder builder = new StringBuilder();
        String docbookCatalogName = "docbook/catalog.xml";
        URL docbookCatalog = classLoader.getResource(docbookCatalogName);

        if (docbookCatalog == null) {
            throw new IllegalStateException("Docbook catalog " + docbookCatalogName + " could not be found in " + classLoader);
        }

        builder.append(docbookCatalog.toExternalForm());

        Enumeration enumeration = classLoader.getResources("/catalog.xml");
        while (enumeration.hasMoreElements()) {
            builder.append(';');
            URL resource = (URL) enumeration.nextElement();
            builder.append(resource.toExternalForm());
        }
        String catalogFiles = builder.toString();
        manager.setCatalogFiles(catalogFiles);
        return manager;
    }

    protected void preFOPTransform(Transformer transformer, File sourceFile, File outputFile) {
    }

    /**
     * <a href="http://xmlgraphics.apache.org/fop/0.95/embedding.html#render">From the FOP usage guide</a>
     */
    protected void postFOPTransform(File foFile) {
//        String imagesPath = copyImages(project, xdir)

        FopFactory fopFactory = FopFactory.newInstance();

        fopFactory.setBaseURL(project.file("$targetDir").toURI().toURL().toExternalForm());
        fopFactory.setFontBaseURL(project.file("$targetDir/fonts").toURI().toURL().toExternalForm());

        fopFactory.setUserConfig(project.file("$targetDir/fop/fop-userconfig.xml"));


        OutputStream out = null;
        String outputFilename = foFile.getName().substring(0, foFile.getName().lastIndexOf(".")) + '.' + extension;

        final File targetFile = new File(foFile.getParentFile(), outputFilename);
        logger.debug("Transforming 'fo' file " + foFile + " to target file: " + targetFile);

        try {
            out = new BufferedOutputStream(new FileOutputStream(targetFile));

            Fop fop = fopFactory.newFop(targetMime, out);

            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer();

            Source src = new StreamSource(foFile);
            src.setSystemId(foFile.toURI().toURL().toExternalForm());

            Result res = new SAXResult(fop.getDefaultHandler());

            /* switch (project.gradle.startParameter.logLevel) {
                 case LogLevel.DEBUG:
                 case LogLevel.INFO:
                     break;
                 default:
                     // only show verbose fop output if the user has specified 'gradle -d' or 'gradle -i'
                     LoggerFactory.getILoggerFactory().getLogger('org.apache.fop').level =
                             LoggerFactory.getILoggerFactory().getLogger('ROOT').level.class.ERROR
             }*/

            transformer.transform(src, res);

        } finally {
            if (out != null) {
                out.close();
            }
        }

        if (!foFile.delete()) {
            logger.warn("Failed to delete 'fo' file " + foFile);
        }

//        if (!project.delete(imagesPath)) {
//            logger.warn("Failed to delete 'images' path " + imagesPath);
//        }
    }

}
