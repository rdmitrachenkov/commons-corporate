package com.rooxteam.corporate

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.publish.maven.MavenPublication

/**
 * Declares provided dependency scope, configures sources and javadoc publication.
 */
class RooxWarPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.apply(plugin: 'roox-corporate');
        project.apply(plugin: 'maven-publish');
        project.apply(plugin: 'war');
        publishWar(project);
    }


    void publishWar(Project p) {

        p.publishing {
            publications {
                war(MavenPublication) {
                    artifact p.war {
                        classifier = 'war'
                    }
                }
            }
        }
    }
}