package com.rooxteam.corporate

import org.gradle.tooling.BuildException

/**
 * Project version
 */
class Version {

    Integer major;

    Integer minor;

    Integer patch;

    /**
     * BUILD_NUMBER or special value like SNAPSHOT
     */
    def build;

    Version(String version) {
        if (version == null) {
            version = 'undefined';
        }

        if (version.contains('-')) {
            build = version.split('-')[1];
            try {
                build = build as Integer;
            } catch (e) {
            }
            version = version.split('-')[0];
        }
        String[] parts = version.split('\\.', 3);
        if (parts.length > 0) {
            try {
                major = parts[0] as Integer;
            } catch (e) {
                // treat whole version as build
                build = version;
                return;
            }
        }
        if (parts.length > 1) {
            minor = parts[1] as Integer;
        }
        if (parts.length > 2) {
            patch = parts[2] as Integer;
        }
        if (parts.length > 3) {
            throw new BuildException("Version should not contain more then 3 parts beside build-number");
        }
    }

    boolean isRelease() {
        return build instanceof Number;
    }

    String toString() {
        return '' + (major != null ? major : '') +
                (minor != null ? '.' + minor : '') +
                (patch != null ? '.' + patch : '') +
                ((major != null || minor != null || patch != null) ? '-' : '') +
                (build != null ? build : 'SNAPSHOT');
    }

    String getWithoutBuild() {
        return '' + (major != null ? major : '') +
                (minor != null ? '.' + minor : '') +
                (patch != null ? '.' + patch : '');
    }

    /**
     * Возвращает версию в формате major.minor, остальная часть обрезается
     * @return
     */
    String getMajorMinor() {
        return '' + (major != null ? major : '') +
                (minor != null ? '.' + minor : '');
    }


    /**
     * Возвращает версию в формате "majorminor" (без точки!), остальная часть обрезается
     * @return
     */
    String getMajorMinorWithoutDot() {
        return '' + (major != null ? major : '') +
                (minor != null ? '' + minor : '');
    }

}
