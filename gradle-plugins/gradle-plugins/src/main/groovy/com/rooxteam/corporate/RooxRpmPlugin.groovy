package com.rooxteam.corporate

import com.netflix.gradle.plugins.rpm.Rpm
import com.netflix.gradle.plugins.rpm.RpmPlugin
import org.apache.tools.ant.filters.FixCrLfFilter
import org.apache.tools.ant.filters.ReplaceTokens
import org.gradle.api.Project
import org.gradle.api.internal.ConventionMapping
import org.gradle.api.internal.IConventionAware
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.tasks.Copy
import org.redline_rpm.header.Os
import org.redline_rpm.payload.Directive

/**
 * Provides task {@link com.rooxteam.corporate.RooxRpmPlugin.RooxRpm} with preconfigured
 * <ul>
 *   <li> auto replacing serviceName, ports and custom properties <code>%prop%</code> in /src/main/conf.
 *        (custom property map can be set with <code>rpmPlugin.tokens</code>)
 *   <li> rpm headers
 *   <li> pre/post install/remove scripts
 *   <li> common copy actions
 *   <li> RPM publication
 * </ul>
 *
 * @author Alexander Katanov
 * @author Dmitry Tikhonov
 */
public class RooxRpmPlugin extends RpmPlugin {

    private static Logger logger = Logging.getLogger(RooxRpmPlugin);

    @Override
    public void apply(Project project) {
        super.apply(project);
        project.apply(plugin: 'roox-corporate');
        project.apply(plugin: 'maven-publish');

        project.ext.RooxRpm = RooxRpm.class;
        project.extensions.create("rpmPlugin", RooxRpmExtension)
    }

    public static class RooxRpmExtension {
        def Map<String, String> tokens = Collections.emptyMap()
        def String serviceName
        def boolean includeLogConfig = true
        def boolean includeSystemdConfig = false
        def boolean includeWar = true

        @Override
        public String toString() {
            return "RooxRpmExtension{" +
                    "tokens=" + tokens +
                    ", serviceName='" + serviceName + '\'' +
                    ", includeLogConfig=" + includeLogConfig +
                    ", includeSystemdConfig=" + includeSystemdConfig +
                    ", includeWar=" + includeWar +
                    '}';
        }
    }

    public static class RooxRpm extends Rpm {

        private static Logger logger = Logging.getLogger(RooxRpmPlugin);

        def component = guessComponent()
        def serviceName = CorporateGradlePlugin.guessServiceName(project)

        @Override
        protected void applyConventions() {
            super.applyConventions()

            project.task('processConfig', type: Copy) {
                filter(ReplaceTokens, beginToken: '%', endToken: '%', tokens: replaceConfigTokens());
                from('src/main/conf/')
                from('src/main/rpm/')
                into('build/conf/')
            }.doLast({

            })
            this.doFirst({
                addRpmScripts('build/conf')
                addFileCopying()
            })
            dependsOn('processConfig')
            addRpmProperties()

            addPublications()
        }


        protected def replaceConfigTokens() {
            def substitutionMap = [:];
            substitutionMap.putAll(portNumbers());
            substitutionMap.put('serviceName', serviceName);
            substitutionMap.putAll(project.rpmPlugin.tokens);
            return substitutionMap;
        }

        void addRpmScripts(String baseDir) {
            project.logger.debug("in addRpmScripts");
            if (!baseDir.endsWith('/') && !baseDir.endsWith('\\')) {
                baseDir += '/'
            }
            project.logger.debug("using baseDir ${baseDir}");

            def preInstall = project.file(baseDir + 'preinstall.sh')
            if (preInstall.exists()) {
                project.logger.debug("adding preInstall from ${preInstall}");
                exten.preInstall(preInstall)
            } else {
                project.logger.debug("skipping preInstall from ${preInstall} because it doesn't exist");
            }

            def postInstall = project.file(baseDir + 'postinstall.sh')
            if (postInstall.exists()) {
                project.logger.debug("adding postInstall from ${postInstall}");
                exten.postInstall(postInstall)
            } else {
                project.logger.debug("skipping postInstall from ${postInstall} because it doesn't exist");
            }

            def preRemove = project.file(baseDir + 'preremove.sh')
            if (preRemove.exists()) {
                project.logger.debug("adding preRemove from ${preRemove}");
                exten.preUninstall(preRemove)
            } else {
                project.logger.debug("skipping preRemove from ${preRemove} because it doesn't exist");
            }

            def postRemove = project.file(baseDir + 'postremove.sh')
            if (postRemove.exists()) {
                project.logger.debug("adding postRemove from ${postRemove}");
                exten.postUninstall(postRemove)
            } else {
                project.logger.debug("skipping postRemove from ${postRemove} because it doesn't exist");
            }

            def utils = project.file(baseDir + 'utils.sh')
            if (utils.exists()) {
                project.logger.debug("adding utils from ${utils}");
                exten.installUtils(utils)
            } else {
                project.logger.debug("skipping utils from ${utils} because it doesn't exist");
            }
        }

        private void addRpmProperties() {
            ConventionMapping mapping = ((IConventionAware) this).getConventionMapping()

            mapping.map('classifier', { '' })
            mapping.map('release', { project.version.build as String ?: 'SNAPSHOT' })
            mapping.map('version', { project.version.getWithoutBuild() })
            mapping.map('license', { 'Proprietary Software' })
            mapping.map('packageGroup', { 'Application/Proprietary' })
            mapping.map('vendor', { 'RooX Solutions' })
            mapping.map('url', { 'http://rooxteam.com/' })
            mapping.map('os', { parentExten?.getOs() ?: Os.LINUX })
            mapping.map('packageName', { "${project.name}-${project.version.majorMinorWithoutDot}" as String })
        }

        protected void addFileCopying() {
            def serviceName = replaceConfigTokens().get('serviceName') as String;

            if (project.rpmPlugin.includeSystemdConfig) {
                def serviceNameEscaped = serviceName.replaceAll( '-', '/' );
                from("build/conf/systemd.template") {
                    into('/etc/sysconfig')
                    setFileMode(0400)
                    setFileType(Directive.CONFIG)
                    setCreateDirectoryEntry(false)
                    setUser('root')
                    setPermissionGroup('root')
                    rename('systemd.template', "tomcat@" + serviceNameEscaped)
                    filter(FixCrLfFilter,
                            eol: FixCrLfFilter.CrLf.newInstance('lf'),
                            tab: FixCrLfFilter.AddAsisRemove.newInstance('asis'),
                            eof: FixCrLfFilter.AddAsisRemove.newInstance('remove'),
                            fixlast: true)
                }
            }

            from("build/conf/service.template") {
                into('/etc/sysconfig')
                setFileMode(0400)
                setFileType(Directive.CONFIG)
                setCreateDirectoryEntry(false)
                setUser('root')
                setPermissionGroup('root')
                rename('service.template', serviceName)
                filter(FixCrLfFilter,
                        eol: FixCrLfFilter.CrLf.newInstance('lf'),
                        tab: FixCrLfFilter.AddAsisRemove.newInstance('asis'),
                        eof: FixCrLfFilter.AddAsisRemove.newInstance('remove'),
                        fixlast: true)
            }

            into("/opt/$serviceName")

            setUser('tomcat')
            setPermissionGroup('tomcat')

            if (project.hasProperty("war") && project.rpmPlugin.includeWar) {
                from(project.war) {
                    into('webapps')
                    setFileMode(0644)
                    setCreateDirectoryEntry(false)
                }
            }

            if (project.rpmPlugin.includeLogConfig) {
                from('src/main/resources/log4j.xml') {
                    into("conf/$serviceName")
                    setFileMode(0444)
                    setFileType(new Directive(Directive.RPMFILE_CONFIG | Directive.RPMFILE_NOREPLACE))
                    setCreateDirectoryEntry(false)
                }

                from('src/main/resources/logback.xml') {
                    into("conf/$serviceName")
                    setFileMode(0444)
                    setFileType(new Directive(Directive.RPMFILE_CONFIG | Directive.RPMFILE_NOREPLACE))
                    setCreateDirectoryEntry(false)
                }
            }

            from('build/conf/server.xml') {
                into('conf')
                setFileMode(0444)
                setFileType(new Directive(Directive.RPMFILE_CONFIG | Directive.RPMFILE_NOREPLACE))
                setCreateDirectoryEntry(false)
            }
            from('build/conf/context.xml') {
                into('conf')
                setFileMode(0444)
                setFileType(new Directive(Directive.RPMFILE_CONFIG | Directive.RPMFILE_NOREPLACE))
                setCreateDirectoryEntry(false)
            }
            from('src/main/rpm_config_override/') {
                setFileMode(0444)
                setFileType(new Directive(Directive.RPMFILE_CONFIG))
                setCreateDirectoryEntry(false)
            }
        }

        private void addPublications() {
            project.publishing {
                publications {
                    rpm(MavenPublication) {
                        artifact project.makeRpm {
                            classifier = 'rpm'
                        }
                    }
                }
            }
        }

        public def portNumbers() {
            def ports = [:]
            if (component != null) {
                Listener.values().each {
                    ports.put(it.name().toLowerCase() + '_port',
                            String.valueOf(Ports.getPort(component, project.version as Version, it)))
                }

                def httpPort = String.valueOf(Ports.getPort(component, project.version as Version, Listener.HTTP))
                ports.put('port_prefix', httpPort.substring(0, httpPort.length() - 1))
            }
            return ports
        }

        private Component guessComponent() {
            logger.error("\t RPM RPLUGIN rpmPlugin" + project.hasProperty("rpmPlugin"))

            if (project.hasProperty('component')) {
                return project.ext.component
            }

            Component guessed = null
            Component.values().each {
                if (project.name.contains(it.name().toLowerCase())) {
                    guessed = it
                }
            }

            if (guessed == null) {
                logger.warn('Cannot guess project component, please set `project.ext.component` property.')
            } else {
                logger.info("Guessed component name: $guessed")
            }
            return guessed
        }

        protected String getServiceName(project, defaultServiceName) {
            def result = defaultServiceName;
            if (project.rpmPlugin.serviceName != null) {
                result = project.rpmPlugin.serviceName;
            }
            def tokenServiceName = replaceConfigTokens().get('serviceName');
            if (tokenServiceName != null) {
                result = tokenServiceName;
            }
            return result;
        }
    }
}

