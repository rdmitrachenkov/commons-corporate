package com.rooxteam.corporate

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging

/**
 * Предоставляет функциональность экспорта документации в Confluence
 * @author kkorsakov
 */
public class ConfluencePlugin implements Plugin<Project> {

    private static Logger logger = Logging.getLogger(ConfluencePlugin);

    @Override
    public void apply(Project project) {
        project.apply(plugin: 'base')
        project.extensions.create("confluence", ConfluenceExtension)
    }

    public static class ConfluenceExtension {

        /**
         * Default space to upload pages to. May be overriden in tasks
         */
        String space = "JD"

        /**
         * REST API URL up to '/api' part like https://rooxteam.jira.com/wiki/rest/api/
         */
        String apiUrl = "https://rooxteam.jira.com/wiki/rest/api/"

        /**
         * Username from whom to post content to wiki
         */
        String username;

        /**
         * User's password
         */
        String password;

    }
}

