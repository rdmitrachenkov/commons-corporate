package com.rooxteam.corporate

/**
 * Standard ports assignment for RX components
 */
class Ports {

    /**
     * Calculates component listening port based on RX convention
     * @param component component id
     * @param version version of component
     * @param listener listener id
     * @return
     */
    static int getPort(Component component, Version version, Listener listener){
        if(component==null){
            throw new IllegalArgumentException("component")
        }
        if(version==null){
            throw new IllegalArgumentException("version")
        }
        if(listener==null){
            throw new IllegalArgumentException("listener")
        }
        return component.id * 1000 + (version.major % 10) * 100 + (version.minor % 10) * 10 + listener.id
    }
}
