package com.rooxteam.corporate

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential
import com.google.api.client.http.HttpTransport
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.drive.Drive
import com.google.api.services.drive.DriveScopes
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging

import java.security.GeneralSecurityException

/**
 * Предоставляет функциональность аплоада файлов на Google Drive
 * @author kkorsakov
 */
public class GoogleDrivePlugin implements Plugin<Project> {

    private static Logger logger = Logging.getLogger(GoogleDrivePlugin);

    @Override
    public void apply(Project project) {
        project.apply(plugin: 'base')
        project.extensions.create("googleDrive", GoogleDriveExtension)
    }

/*
    public static Drive getDriveService() throws GeneralSecurityException,
            IOException, URISyntaxException {
        HttpTransport httpTransport = new NetHttpTransport();
        JacksonFactory jsonFactory = new JacksonFactory();
        GoogleCredential credential = new GoogleCredential.Builder()
                .setTransport(httpTransport)
                .setJsonFactory(jsonFactory)
                .setServiceAccountId(SERVICE_ACCOUNT_EMAIL)
                .setServiceAccountScopes(DriveScopes.DRIVE)
                .setServiceAccountPrivateKeyFromP12File(
                new java.io.File(SERVICE_ACCOUNT_PKCS12_FILE_PATH))
                .build();
        Drive service = new Drive.Builder(httpTransport, jsonFactory, null)
                .setHttpRequestInitializer(credential).build();
        return service;
    }
*/


    public static class GoogleDriveExtension {

        public static final String ROOT_FOLDER = "root"

        /**
         * Default folder to upload files to. May be overriden in tasks.
         * By default is root.
         */
        String folder = ROOT_FOLDER


    }
}

