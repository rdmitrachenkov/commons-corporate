package com.rooxteam.corporate

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.file.DuplicatesStrategy
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging

/**
 * Creates RPM package for WebAPI on Spring Boot.
 * <ul>
 * <li>Declares all necessary dependencies</li>
 * <li>Extracts WebapiApplication starter and root-package resources from webapi-common</li>
 * <li>Applies spring-boot plugin and builds executable jar</li>
 * <li>Builds RPM package with jar, service script, standard postinstall/preremove</li>
 * </ul>
 */
class WebapiPlugin implements Plugin<Project> {

    private static final Logger logger = Logging.getLogger(WebapiPlugin);

    static class WebapiPluginExtension {
        def String platformVersion = '1.1+'
        def boolean useStandardService = true
        def boolean useStandardScripts = true
        def String maxHeapSize = "2g"
        def String threadStackSize = "1m"

        /**
         * Additional options passed to JVM as is
         */
        def String options = ""
        /**
         * choose spring profiles
         */
        def String springProfiles = ""

        @Override
        public String toString() {
            return "WebapiPluginExtension{" +
                    "platformVersion=" + platformVersion +
                    ", useStandardService=" + useStandardService +
                    ", useStandardScripts=" + useStandardScripts +
                    '}';
        }
    }

    @Override
    void apply(Project project) {
        project.extensions.create("webapi", WebapiPluginExtension)

        project.apply(plugin: 'roox-jar')
        project.apply(plugin: 'roox-rpm')
        project.apply(plugin: 'spring-boot')

        configureDependencies(project)
        configureRpm(project)
        extractCommonFromJar(project)

        project.task('makeRpm', type: WebapiRpmTask, dependsOn: ['processConfig', 'jar', 'bootRepackage'])
        project.build.dependsOn('makeRpm')
    }

    void configureDependencies(project) {
        project.dependencies {
            compile(group: 'com.rooxteam.webapi', name: 'webapi-platform', version: project.webapi.platformVersion)
        }
        project.task('webapiDependencies')
        project.tasks.compileJava.dependsOn('webapiDependencies')
    }

    void configureRpm(project) {
        project.jar {
            baseName = 'roox-webapi'
            manifest {
                attributes('Implementation-Title': 'Roox WebAPI', 'Implementation-Version': project.version)
            }
        }
    }

    void extractCommonFromJar(Project project) {
        project.task('extractCommonFromJar') << {
            def webapiCommon = project.configurations.compile.find {
                it.name.startsWith("webapi-common")
            }

            project.copy {
                from(project.zipTree(webapiCommon)) {
                    include('**/WebapiApplication.class')
                }
                into('build/classes/main')
                setIncludeEmptyDirs(false)
                setDuplicatesStrategy(DuplicatesStrategy.EXCLUDE)
            }

            project.copy {
                from(project.zipTree(webapiCommon))
                include('*.*')
                into('build/resources/main')
                setIncludeEmptyDirs(false)
                setDuplicatesStrategy(DuplicatesStrategy.EXCLUDE)
                exclude('log4j.xml')
                exclude('app.properties')
            }
        }
        project.tasks.compileJava.dependsOn('extractCommonFromJar')
        project.tasks.processResources.dependsOn('extractCommonFromJar')
        project.tasks.jar.dependsOn('extractCommonFromJar')
    }
}
