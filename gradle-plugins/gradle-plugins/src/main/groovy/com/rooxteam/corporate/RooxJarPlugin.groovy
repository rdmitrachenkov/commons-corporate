package com.rooxteam.corporate

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.tasks.bundling.Jar

/**
 * Declares provided dependency scope, configures sources and javadoc publication.
 */
class RooxJarPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.apply(plugin: 'roox-corporate');
        project.apply(plugin: 'maven-publish');

        project.allprojects {
            apply plugin: 'roox-corporate'
            apply plugin: 'maven-publish'

            jar.archiveName = "${project.name}-${project.version.majorMinor}.jar"
        }

        createProvidedConfiguration(project);
        publishSourcesAndJavadocs(project);
    }

    void createProvidedConfiguration(Project p) {
        p.configurations {
            provided
        }

        p.sourceSets.main.compileClasspath += [p.configurations.provided]
        p.sourceSets.test.compileClasspath += [p.configurations.provided]
        p.sourceSets.test.runtimeClasspath += [p.configurations.provided]

        p.apply(plugin: 'idea');
        p.apply(plugin: 'eclipse');
        p.idea.module { scopes.PROVIDED.plus += [p.configurations.provided] }
        p.eclipse.classpath { plusConfigurations += [p.configurations.provided] }
    }

    void publishSourcesAndJavadocs(Project p) {
        p.javadoc {
            classpath = p.sourceSets.main.compileClasspath
            source = p.sourceSets.main.allJava
            options {
                encoding = 'UTF-8'
            }
            failOnError = false
        }

        p.task([type: Jar], 'sourceJar') {
            classifier = 'sources';
            from(p.sourceSets.main.allSource);
        }

        p.task([type: Jar, dependsOn: p.javadoc], 'javadocJar') {
            classifier = 'javadoc';
            from(p.javadoc.destinationDir);
        }

        p.build.dependsOn('javadocJar');
        p.build.dependsOn('sourceJar');

        p.publishing {
            publications {
                sources(MavenPublication) {
                    from(p.components.java);
                    artifact(p.sourceJar) {
                        classifier = 'sources';
                    }
                }

                javadoc(MavenPublication) {
                    from(p.components.java);
                    artifact(p.javadocJar) {
                        classifier = 'javadoc';
                    }
                }
            }
        }
    }
}