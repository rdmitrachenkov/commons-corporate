package com.rooxteam.corporate

import org.apache.tools.ant.filters.FixCrLfFilter
import org.apache.tools.ant.filters.ReplaceTokens
import org.gradle.api.DefaultTask
import org.gradle.api.file.DuplicatesStrategy
import org.gradle.api.internal.ConventionMapping
import org.gradle.api.internal.IConventionAware
import org.gradle.api.tasks.Copy

class WebapiRpmTask extends RooxRpmPlugin.RooxRpm {
    @Override
    protected void applyConventions() {
        super.applyConventions()

        ConventionMapping mapping = ((IConventionAware) this).getConventionMapping()

        String customerCode = project.hasProperty('customer') ? project.customer.code.toUpperCase() : 'ROOX'
        mapping.map('distribution', { "$customerCode-$project.version" as String })

        def pluginJar = getClass().getClassLoader().getResource("rpm/webapi").getFile().replaceAll('(.+?)!.*', '$1')

        project.task('extractWebapiResources', type: Copy) {
            from(project.zipTree(pluginJar))
            include('rpm/webapi/*')
            into('build/confTemplate')
        }
        dependsOn('extractWebapiResources')

        project.afterEvaluate({

            project.task('processWebapiConfig', type: Copy, dependsOn: ['processConfig', 'extractWebapiResources']) {
                filter(ReplaceTokens, beginToken: '%', endToken: '%', tokens: replaceConfigTokens());
                from('build/confTemplate/rpm/webapi')
                into('build/conf')
                duplicatesStrategy = DuplicatesStrategy.EXCLUDE
            }.doLast({
                addRpmScripts('build/conf')
            })

            from('build/conf/boot-service.template') {
                into('/etc/init.d')
                setFileMode(0755)
                setFileType(CONFIG)
                setCreateDirectoryEntry(false)
                setUser('root')
                setPermissionGroup('root')

                rename('boot-service.template', getServiceName(project, serviceName))
                filter(FixCrLfFilter,
                        eol: FixCrLfFilter.CrLf.newInstance('lf'),
                        tab: FixCrLfFilter.AddAsisRemove.newInstance('asis'),
                        eof: FixCrLfFilter.AddAsisRemove.newInstance('remove'),
                        fixlast: true)
            }

            dependsOn('processWebapiConfig');
        });



        from(project.jar) {
            into('libs')
            setCreateDirectoryEntry(false)
            setFileMode(0644)
        }
    }

    @Override
    protected replaceConfigTokens() {
        def tokens = super.replaceConfigTokens();
        if (!tokens.containsKey('jarFile')) {
            tokens.put('jarFile', "roox-webapi-${project.version.majorMinor}" as String);
        }
        if (!tokens.containsKey('contextPath')) {
            tokens.put('contextPath', "webapi-${project.version.majorMinor}" as String);
        }
        tokens.put('maxHeapSize', project.webapi.maxHeapSize);
        tokens.put('threadStackSize', project.webapi.threadStackSize);
        tokens.put("springProfiles",project.webapi.springProfiles);
        tokens.put("options",project.webapi.options);
        return tokens;
    }
}
