package com.rooxteam.corporate

import com.rooxteam.config.Customer
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.DependencyResolveDetails

// configures build environment
class CorporateGradlePlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        if (!(project.version instanceof Version)) {
            project.version = new Version(project.version);
        }
        def buildNumber = System.getenv('BUILD_NUMBER')
        if (buildNumber != null) {
            project.version.build = buildNumber as Integer;
        }

        guessCustomer(project);
        configureCustomerSpecificProperties(project);
        configureJavaLevels(project);
        configureRepositories(project);
        configurePublications(project);
        configureDependencies(project);

        project.task('rx-customer-info') << {
            if (!project.hasProperty('customer') || !project.ext.customer) {
                println('This project is not a customer project. You can set customer in project.ext.customerCode property or ' +
                        'set project.name based on convention where project name suffix is \'-customerCode\' like \'webapi-server-roox\'')
            } else {
                Customer cus = project.ext.customer;
                println("""

Customer:
        Code: ${cus.code ?: ''}
        Display Name: ${cus.displayName ?: ''}
        Icon: ${cus.iconUrl ?: ''}
        Comments: ${cus.comments ?: ''}


Repositories:
        EAP: ${project.ext.customerEap ?: ''}
        Releases: ${project.ext.customerReleases ?: ''}
                """);
            }
        }

        project.configure(project.allprojects) {
            afterEvaluate { p ->
                def serviceName = guessServiceName(project);
                if (p.hasProperty("war")) {
                    p.war {
                        manifest {
                            attributes(
                                    "X-RooX-Solutions-Build-Tag": System.getenv("BUILD_TAG") ?: "DEVELOPER_BUILD",
                                    "X-RooX-Solutions-Build-Number": System.getenv("BUILD_NUMBER") ?: "SNAPSHOT",
                                    "X-RooX-Solutions-Build-Id": System.getenv("BUILD_ID") ?: "DEVELOPER_BUILD",
                                    "X-RooX-Solutions-Commit": System.getenv("GIT_COMMIT") ?: "DEVELOPER_BUILD",
                                    "X-RooX-Solutions-Branch": System.getenv("GIT_BRANCH") ?: "DEVELOPER_BUILD",
                                    "X-RooX-Solutions-Sources-Repo": System.getenv("GIT_URL") ?: "DEVELOPER_BUILD",
                                    "X-RooX-Solutions-Component-Id": p.name,
                                    "X-RooX-Solutions-Component-Version": version,
                                    "X-RooX-Solutions-Component-Name": serviceName
                            )
                        }
                    }
                }

                if (p.hasProperty("jar")) {
                    p.jar {
                        manifest {
                            attributes(
                                    "Archiver-Version": "Gradle ${gradle.gradleVersion}",
                                    "Created-By": "Gradle",
                                    "Built-By": System.getProperty('user.name'),
                                    "Built-Jdk": System.getProperty('java.version'),
                                    "X-RooX-Solutions-Build-Tag": System.getenv("BUILD_TAG") ?: "DEVELOPER_BUILD",
                                    "X-RooX-Solutions-Build-Number": System.getenv("BUILD_NUMBER") ?: "SNAPSHOT",
                                    "X-RooX-Solutions-Build-Id": System.getenv("BUILD_ID") ?: "DEVELOPER_BUILD",
                                    "X-RooX-Solutions-Commit": System.getenv("GIT_COMMIT") ?: "DEVELOPER_BUILD",
                                    "X-RooX-Solutions-Branch": System.getenv("GIT_BRANCH") ?: "DEVELOPER_BUILD",
                                    "X-RooX-Solutions-Sources-Repo": System.getenv("GIT_URL") ?: "DEVELOPER_BUILD",
                                    "X-RooX-Solutions-Component-Id": p.name,
                                    "X-RooX-Solutions-Component-Version": version,
                                    "X-RooX-Solutions-Component-Name": serviceName
                            )
                        }
                    }
                }
            }
        }
    }

    def configureCustomerSpecificProperties(Project p) {
        if (!p.hasProperty('customer')) {
            return;
        }
        Customer customer = p.properties['customer'];
        p.configure(p.allprojects) {
            ext.customerDisplayName = customer.displayName;
            ext.customerIconUrl = customer.iconUrl;
            ext.customerCode = customer.code;
        }
    }

    def configureJavaLevels(Project p) {
        p.configure(p.allprojects) {
            apply(plugin: 'java')
            targetCompatibility = 1.7
            sourceCompatibility = 1.7
            if (compileJava != null) {
                compileJava.options.encoding = 'UTF-8'
            }
            if (compileTestJava != null) {
                compileTestJava.options.encoding = 'UTF-8'
            }
        }
    }

    def configureDependencies(Project p) {
        p.ext.corporateDependencies = Dependencies.preferredDependencies;

        p.configurations.all {
            resolutionStrategy {
                eachDependency { DependencyResolveDetails details ->
                    String forced = p.ext.corporateDependencies["$details.requested.group:$details.requested.name"]
                    if (forced) {
                        if (!details.requested.version) {
                            p.logger.info("providing default version $details.requested.group:$details.requested.name:$forced")
                            details.useVersion(forced)
//                        todo: current version comparator is not working for gradle 2.x, need to find alternative.
//                        } else if (Dependencies.versionComparator.compare(details.requested.version, forced) < 0) {
//                            p.logger.info("forced changing version of $details.requested.group:$details.requested.name" +
//                                    " from $details.requested.version to $forced")
//                            details.useVersion(forced)
                        } else {
                            p.logger.info("using explicit version $details.requested")
                        }
                    }
                }
            }
        }
    }

    def configureRepositories(Project p) {
        p.ext {
            rooxRepoBase = "http://nexus.rooxintra.net/content/repositories/";
            customerRepoBase = "http://repo.rooxteam.com/customers/content/repositories/";
            rooxPublic = rooxRepoBase + "public";
            rooxReleases = rooxRepoBase + "releases";
            rooxSnapshots = rooxRepoBase + "snapshots";

            if (p.hasProperty('rooxExternalDevPublic')) {
                rooxExternalDevPublic = p.ext.rooxExternalDevPublic;
            } else {
                rooxExternalDevPublic = "http://repo.rooxteam.com/customers/content/groups/public"
            }
            if (p.hasProperty('rooxExternalDevReleases')) {
                rooxExternalDevReleases = p.ext.rooxExternalDevReleases;
            } else {
                rooxExternalDevReleases = customerRepoBase + "roox.releases"
            }
            if (p.hasProperty('rooxExternalDevSnapshot')) {
                rooxExternalDevSnapshot = p.ext.rooxExternalDevSnapshot;
            } else {
                rooxExternalDevSnapshot = customerRepoBase + "roox.snapshots"
            }

            String task = p.hasProperty('roox_task') ? p.roox_task : System.getenv("roox_task")
            if (task != null && !task.trim().isEmpty()) {
                rooxTaskReleases = rooxRepoBase + task + ".releases";
                rooxTaskSnapshots = rooxRepoBase + task + ".snapshots";
            }

            // customers
            if (p.hasProperty('customer') && p.ext.customer) {
                customerEap = customerRepoBase + "customers." + customer.code + ".eap";
                customerReleases = customerRepoBase + "customers." + customer.code + ".releases";
            }
        }

        p.configure(p.allprojects) {
            repositories {
                mavenLocal()
                if (!p.hasProperty('rooxExternalAccess')) {
                    maven {
                        url = p.rooxPublic
                        println "Enabling rooxPublic as $url"
                    }
                    maven {
                        url = p.rooxReleases
                        println "Enabling rooxReleases as $url"
                    }
                    maven {
                        url = p.rooxSnapshots
                        println "Enabling rooxSnapshots as $url"
                    }
                }else{
                    println "Enabling mavenLocal because running in restricted env"
                    mavenLocal();
                    println "Enabling mavenCentral because running in restricteed env"
                    mavenCentral();
                }
                if (p.hasProperty('rooxUser') && p.hasProperty('rooxPassword')) {
                    maven {
                        url = p.rooxExternalDevReleases
                        println "Enabling rooxExternalDevReleases as ${url}"
                        credentials {
                            username p.ext.rooxUser
                            password p.ext.rooxPassword
                        }
                    }
                }
                if (p.hasProperty('rooxUser') && p.hasProperty('rooxPassword')) {
                    maven {
                        url = p.rooxExternalDevPublic
                        println "Enabling rooxExternalDevPublic as ${url}"
                        credentials {
                            username p.ext.rooxUser
                            password p.ext.rooxPassword
                        }
                    }
                }
                if (p.hasProperty('rooxUser') && p.hasProperty('rooxPassword')) {
                    maven {
                        url = p.rooxExternalDevSnapshot
                        println "Enabling rooxExternalDevSnapshot as ${url}"

                        credentials {
                            username p.ext.rooxUser
                            password p.ext.rooxPassword
                        }
                    }
                }
                if (p.hasProperty('rooxTaskReleases')) {
                    maven {
                        url = p.rooxTaskReleases
                        println "Enabling rooxTaskReleases as ${url}"
                    }
                }
                if (p.hasProperty('rooxTaskSnapshots')) {
                    maven {
                        url = p.rooxTaskSnapshots
                        println "Enabling rooxTaskSnapshots as ${url}"
                    }
                }
            }
        }
    }

    /**
     * Get or guess customer code
     * @param p
     */
    void guessCustomer(Project p) {
        if (p.hasProperty('customerCode')) {
            String attempt = p.ext.customerCode;
            p.logger.debug("Customer Code is set as project property: " + attempt);
            p.ext.customer = Customer.findByCode(attempt);
            return;
        }
        p.logger.debug("Customer Code is not set")
        // guess from name
        if (p.name != null) {
            p.logger.debug("Guessing Customer Code from project name")
            if (p.name.indexOf('-') >= 0) {
                // guess by suffix
                String attempt = p.name.substring(p.name.lastIndexOf('-') + 1);
                Customer c = Customer.findByCode(attempt);
                if (c != null) {
                    p.logger.info("Customer Code guessed from project name: " + c.code)
                    p.logger.info("If it is not preferred behaviour set property 'project.ext.customerCode'")
                    p.ext.customer = Customer.findByCode(attempt);
                    return;
                }
                // guess by prefix
                attempt = p.name.substring(0, p.name.indexOf('-'));
                c = Customer.findByCode(attempt);
                if (c != null) {
                    p.logger.info("Customer Code guessed from project name: " + c.code)
                    p.logger.info("If it is not preferred behaviour set property 'project.ext.customerCode'")
                    p.ext.customer = Customer.findByCode(attempt);
                    return;
                }
            }
        }
        // not guessed by project name
    }

    void configurePublications(Project p) {
        p.apply(plugin: 'maven-publish')

        p.publishing {
            repositories {
                if (!p.hasProperty('rooxTaskReleases') && !p.hasProperty('rooxTaskSnapshots')) {
                    if (p.version.release) {
                        if (p.hasProperty('deployerUser') && p.hasProperty('deployerPassword')) {
                            maven {
                                url "http://nexus.rooxintra.net/content/repositories/releases"
                                credentials {
                                    username p.ext.deployerUser
                                    password p.ext.deployerPassword
                                }
                            }
                        }
                    } else {
                        if (p.hasProperty('deployerUser') && p.hasProperty('deployerPassword')) {
                            maven {
                                url "http://nexus.rooxintra.net/content/repositories/snapshots"
                                credentials {
                                    username p.ext.deployerUser
                                    password p.ext.deployerPassword
                                }
                            }
                        }
                    }
                }
                if (p.hasProperty('rooxTaskReleases') && p.version.release) {
                    maven {
                        url p.rooxTaskReleases
                        if (p.hasProperty('deployerUser') && p.hasProperty('deployerPassword')) {
                            credentials {
                                username p.ext.deployerUser
                                password p.ext.deployerPassword
                            }
                        }
                    }
                }
                if (p.hasProperty('rooxTaskSnapshots') && !p.version.release) {
                    maven {
                        url p.rooxTaskSnapshots
                        if (p.hasProperty('deployerUser') && p.hasProperty('deployerPassword')) {
                            credentials {
                                username p.ext.deployerUser
                                password p.ext.deployerPassword
                            }
                        }
                    }
                }
            }
        }
    }

    public static String guessServiceName(Project project) {
        if (project.hasProperty('serviceName')) {
            project.logger.info("Using explicit serviceName `$project.ext.serviceName`.")
            return project.ext.serviceName
        }

        if (project.hasProperty('rpmPlugin') && project.rpmPlugin.serviceName != null) {
            project.logger.info("Using explicit serviceName `$project.rpmPlugin.serviceName`.")
            return project.rpmPlugin.serviceName;
        }

        def rightBorder = project.name.indexOf('-')
        if (rightBorder == -1) {
            rightBorder = project.name.length()
        }
        def serviceName = 'roox-' + project.name.substring(0, rightBorder) + '-' + project.version.majorMinorWithoutDot

        project.logger.info("Using guessed serviceName `$serviceName`.")
        return serviceName
    }
}