package com.rooxteam.corporate

/**
 * Created by kkorsakov on 27.05.2014.
 */
public enum ConfluenceFormat {

    /**
     * Internal storage format (actually well-formed xhtml)
     */
    storage,

    /**
     * Confluence wiki markup format
     */
            wiki

}