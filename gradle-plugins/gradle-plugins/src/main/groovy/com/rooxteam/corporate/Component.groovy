package com.rooxteam.corporate

/**
 * RooX Solutions Components
 */
public enum Component {

    /**
     * WebAPI
     */
    WEBAPI(11),

    /**
     * Widget Rendering System
     */
    WRS(12),

    /**
     * Widget Management System
     */
    WMS(13),

    /**
     * Universal Identity Manager
     */
    UIDM(14),

    /**
     * OpenAM/OpenSSO
     */
    SSO(15),

    /**
     * Push Server
     */
    PUSH(16),

    /**
     * Redirector
     */
    REDIRECTOR(17),

    /**
     * API Gateway server
     */
    GATEWAY(22),

    /**
     * Targeting server
     */
    TARGETING(23);


    Component(int id){
        this.id=id;
    }

    int id;


}