package com.rooxteam.corporate

import org.asciidoctor.Asciidoctor
import org.asciidoctor.AttributesBuilder
import org.asciidoctor.OptionsBuilder
import org.asciidoctor.SafeMode
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.file.FileTree
import org.gradle.api.file.FileTreeElement
import org.gradle.api.tasks.TaskAction

import static org.asciidoctor.Asciidoctor.Factory

/**
 * Created by kkorsakov on 20.05.2014.
 */
class ADocTask extends DefaultTask {

    /**
     * Asciidoctor generator instance
     */
    static final Asciidoctor asciidoctor = Factory.create();

    /**
     * Format for output documents
     */
    def format = DocFormat.html;

    /**
     *  Source files directory
     */
    def sourceDir;

    /**
     * Target files directory
     */
    def targetDir;


    @TaskAction
    def generate() {
        if (format == null) {
            format = DocFormat.html;
        }
        if ((format instanceof CharSequence)) {
            format = DocFormat.valueOf(format.toString());
        }

        if (sourceDir == null) {
            sourceDir = project.adoc.sourceDir;
        }
        if (sourceDir == null) {
            sourceDir = 'src/doc'
        }
        sourceDir = project.file(sourceDir);
        project.logger.debug("Using $sourceDir as sources for documents");
        sourceDir.mkdirs();


        if (targetDir == null) {
            targetDir = project.adoc.targetDir;
        }
        if (targetDir == null) {
            targetDir = new File(project.buildDir, 'doc')
        }
        targetDir = project.file(targetDir);
        project.logger.debug("Using $targetDir as target for documents")
        targetDir.mkdirs();

        project.logger.debug("Copy files from $sourceDir to $targetDir")

        project.fileTree(sourceDir).visit { FileTreeElement element ->
            if (element.file.isFile()) {
                element.copyTo(new File(targetDir.absolutePath + File.separator + element.relativePath.toString()));
            }
        }

        project.logger.debug("Copy styles")
        unpackResources();

        // render files from targetDir as files are copied to it

        // exclude files started with underscore, as it is embed document by convention
        FileTree adFilesOnly = project.fileTree(dir: targetDir,
                includes: ['**/*.ad', '**/*.asciidoc', '**/*.adoc', '**/*.asc'],
                exclude: '**/_*')
        adFilesOnly.visit { FileTreeElement element ->
            if (element.file.isFile()) {
                processFile(element);
            }
        }
    }

    /**
     * Processes single asciidoctor file represented as source FileTreeElement
     * @param element
     */
    protected void processFile(FileTreeElement element) {
        File dstDir = element.relativePath.getFile(targetDir).getParentFile();
        if (!dstDir.exists()) {
            dstDir.mkdirs();
        }
        def dstFileName = element.name.toString();
        dstFileName = dstFileName.substring(0, dstFileName.lastIndexOf(".")) + "." + format.extension;
        def dstFile = new File(dstDir, dstFileName);

        logger.lifecycle("Rendering $element to $dstFile");
        asciidoctor.renderFile(element.file, getOptions(element.file, dstDir));

        postProcessFile(dstFile);
    }

    /**
     * Customize post-rendering of asciidoctor file
     * @param element source file
     * @param dstDir destination directory
     */
    protected void postProcessFile(File file) {

    }

    /**
     * Prepare options to pass to asciidoctor
     * @param format output format (use asciidoctor 'backend' options)
     * @param destinationDir output directory
     * @return options thar are ready to be passed to asciidoctor
     */
    private OptionsBuilder getOptions(File sourceFile, File destinationDir) {
        OptionsBuilder options = OptionsBuilder.options();
        options.backend(format.toString());
        options.inPlace(false)
        options.safe(SafeMode.UNSAFE)
        options.toDir(destinationDir)
        options.baseDir(sourceFile.getParentFile())

        AttributesBuilder attributes = AttributesBuilder.attributes();
        attributes.attribute('projectdir', destinationDir.absolutePath);
        attributes.attribute('rootdir', destinationDir.absolutePath);
        attributes.attribute('project-name', project.name);
        attributes.attribute('project-group', project.group);
        attributes.attribute('project-version', project.version);
        attributes.attribute('project-description', project.description);
        attributes.ignoreUndefinedAttributes(true);
        if (project.hasProperty('customerCode')) {
            attributes.attribute('customer-code', project.properties['customerCode']);
        }
        // commented out as russian text is not handled correctly right now
//        if (project.hasProperty('customerDisplayName')) {
//            attributes.attribute('customer-display-name', project.properties['customerDisplayName'].toString());
//        }
        if (project.hasProperty('customerIconUrl')) {
            attributes.attribute('customer-icon-url', project.properties['customerIconUrl']);
        }
        if (format == DocFormat.pdf) {
            attributes.attributeMissing('customer-icon-url',);
        }
        attributes.experimental(true);
        attributes.linkCss(false);
        attributes.attribute('source-highlighter', 'coderay');
        attributes.styleSheetName("$targetDir/roox.css");
        options.attributes(attributes.asMap());

        return options;
    }

    private void unpackResources() {
        def resourcesZipPath = 'META-INF/roox-resources.zip';
        def resourcesZip = this.class.classLoader.getResource(resourcesZipPath);
        if (resourcesZip == null) {
            throw new GradleException("could not find ${resourcesZipPath} on the classpath");
        }
        // the file is a jar file - write it to disk first
        def zipInputStream = resourcesZip.getContent();
        def zipFile = new File("${project.buildDir}/roox-resources.zip");
        copyFile(zipInputStream, zipFile);
        project.copy {
            from project.zipTree(zipFile);
            into "${targetDir}";
        }
    }

    private void copyFile(InputStream source, File destFile) {
        destFile.createNewFile();
        FileOutputStream to = null;
        try {
            to = new FileOutputStream(destFile);
            byte[] buffer = new byte[4096];
            int bytesRead;

            while ((bytesRead = source.read(buffer)) > 0) {
                to.write(buffer, 0, bytesRead);
            }
        } finally {
            if (source != null) {
                source.close();
            }
            if (to != null) {
                to.close();
            }
        }
    }


}
