package com.rooxteam.corporate

/**
 * Created by kkorsakov on 20.05.2014.
 */
public enum DocFormat {

    /**
     * HTML5
     */
    html("html"),

    /**
     * PDF
     */
            pdf("pdf"),

    docbook("xml");


    String extension;

    DocFormat(String extension) {
        this.extension = extension
    }
}