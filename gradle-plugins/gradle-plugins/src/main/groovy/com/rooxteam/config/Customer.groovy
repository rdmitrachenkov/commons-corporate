package com.rooxteam.config

/**
 * Заказчик RooX Solutions
 */
class Customer {

    /**
     * Короткий код клиента, вроде ocb, yota
     */
    String code;

    /**
     * Иконка - логотип
     */
    String iconUrl;

    /**
     * Полное имя для отображения в интерфейсах
     */
    String displayName;

    /**
     * Примечания
     */
    String comments;

    /**
     * Окружения
     */
    Collection<Environment> environments;

    static Customer findByCode(String code) {
        Customer customer = customers.find {
            if (it.code == code) return code;
        }
        if(customer==null){
            customer = new Customer(code: code, displayName: code)
        }
        return customer;
    }

    static Customer[] customers = [
            new Customer(code: 'yota', displayName: 'Йота', iconUrl: 'http://jenkins.rooxintra.net/userContent/customIcon/a0953da7739e3a34552c75e1c570caa279f407de.png'),
            new Customer(code: 'mts', displayName: 'МТС', iconUrl: 'http://jenkins.rooxintra.net/userContent/customIcon/869cf88bfbc6240e8fc3ac819779172447b7c912.png'),
            new Customer(code: 'mgf', displayName: 'Мегафон', iconUrl: 'http://jenkins.rooxintra.net/userContent/customIcon/7d95cde99da9f71ac7803da8863486fc5bee4cab.png'),
            new Customer(code: 'ocb', displayName: 'Банк Открытие', iconUrl: 'http://jenkins.rooxintra.net/userContent/customIcon/7d48a4c15cc921035a3bebb3929d6832c1c385d9.png'),
            new Customer(code: 'rtk', displayName: 'Ростелеком', iconUrl: 'http://jenkins.rooxintra.net/userContent/customIcon/e119111c2e586a5ff52ec0f6c162529c4bd036ce.png'),
            new Customer(code: 'bwc', displayName: 'Байкалвестком', iconUrl: 'http://jenkins.rooxintra.net/userContent/customIcon/e119111c2e586a5ff52ec0f6c162529c4bd036ce.png'),
            new Customer(code: 'roox', displayName: 'RooX Solutions', iconUrl: 'http://jenkins.rooxintra.net/userContent/customIcon/97c2ac4b9ad07e9f267811c53dcdea5abb347d6b.png'),
            new Customer(code: 'wtl', displayName: 'Whitelabel Customer', iconUrl: 'http://jenkins.rooxintra.net/userContent/customIcon/97c2ac4b9ad07e9f267811c53dcdea5abb347d6b.png'),
            new Customer(code: 'bee', displayName: 'Билайн')
    ]

}
