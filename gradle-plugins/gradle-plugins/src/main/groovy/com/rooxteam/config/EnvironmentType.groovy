package com.rooxteam.config

/**
 * Тип окружения
 */
public enum EnvironmentType {

    PRODUCTION, PREPRODUCTION, DEVELOPMENT, CUSTOMER_PREVIEW
}