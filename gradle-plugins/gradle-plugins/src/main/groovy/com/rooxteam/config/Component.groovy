package com.rooxteam.config

/**
 *
 */
class Component {

    /**
     * like WebAPI
     */
    String name;

    /**
     * long description of component role
     */
    String description;

    /**
     * Component endpoints
     */
    Map<String,Endpoint> endpoints;
}
