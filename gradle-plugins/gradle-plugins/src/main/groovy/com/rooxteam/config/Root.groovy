package com.rooxteam.config

/**
 * How to access root privileges
 */
public enum Root {

    ROOT, SU, SUDO, NO

}