package com.rooxteam.config

/**
 * Окружение
 */
class Environment {

    String name;

    EnvironmentType type;

    Provider provider

    /**
     * username владельца со стороны RS
     */
    String owner;
}
