package com.rooxteam.corporate

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Before
import org.junit.Test

import static org.junit.Assert.assertNotNull
import static org.junit.Assert.assertTrue

/**
 * Created by kkorsakov on 20.05.2014.
 */
class ADocPluginTest {

    Project project

    @Before
    public void init() {
        project = ProjectBuilder.builder().build()
        project.apply plugin: 'roox-doc'
    }

    @Test
    public void plugin_should_add_its_tasks_to_project() {
        assertTrue(project.tasks['adoc-html'] instanceof ADocTask)
        assertTrue(project.tasks['adoc-pdf'] instanceof ADocFOPTask)
        assertTrue(project.tasks['adoc-docbook'] instanceof ADocTask)
        assertNotNull(project.tasks['adoc'])
    }
}
