package com.rooxteam.corporate

import org.junit.Test

/**
 * Created with IntelliJ IDEA.
 * User: kkorsakov
 * Date: 23.11.13
 * Time: 1:04
 * To change this template use File | Settings | File Templates.
 */
class VersionTest {


    @Test
    void test_parsing_null(){
        Version v=new Version(null);
        assert v.toString()=='undefined'
    }

    @Test
    void test_parsing_major(){
        Version v=new Version('1');
        assert v.toString()=='1-SNAPSHOT'
    }

    @Test
    void test_parsing_major_minor(){
        Version v=new Version('1.2');
        assert v.toString()=='1.2-SNAPSHOT'
    }

    @Test
    void test_parsing_major_minor_patch(){
        Version v=new Version('1.2.3');
        assert v.toString()=='1.2.3-SNAPSHOT'
    }

    @Test
    void test_parsing_major_minor_patch_with_build(){
        Version v=new Version('1.2.3-RELEASE');
        assert v.toString()=='1.2.3-RELEASE'
    }


    @Test
    void test_version_with_build_number_is_release(){
        Version v=new Version('1.2.3-12');
        assert v.release;
    }

    @Test
    void test_version_with_snapshot_is_not_a_release(){
        Version v=new Version('1.2.3-SHAPSHOT');
        assert !v.release;
    }


    @Test
    void should_strip_major_minor_for_full_version(){
        Version v=new Version('1.2.3');
        assert v.majorMinor=='1.2'
    }

    @Test
    void should_strip_major_minor_for_full_version_without_dot(){
        Version v=new Version('1.2.3');
        assert v.majorMinorWithoutDot=='12'
    }

    @Test
    void should_handle_null_for_full_version_without_dot(){
        Version v=new Version(null);
        assert v.majorMinor==''
    }
}
