package com.rooxteam.sdk

import org.gradle.api.Project
import org.gradle.api.tasks.Input
import org.gradle.util.ConfigureUtil

public class SDKExtension {

    Project project

    @Input
    CredentialsSpec credentials

    @Input
    PluginSpec plugin

    @Input
    PublishingSpec publishing

    SDKExtension(Project project) {
        this.project = project;
        credentials = new CredentialsSpec(project)
        plugin = new PluginSpec(project, credentials)
        publishing = new PublishingSpec(project)
    }

    CredentialsSpec credentials(Closure configurationClosure) {
        this.credentials = ConfigureUtil.configure(configurationClosure, new CredentialsSpec(project))
        return credentials;
    }

    PluginSpec plugin(Closure configurationClosure) {
        this.plugin = ConfigureUtil.configure(configurationClosure, new PluginSpec(project, credentials))
        return plugin;
    }

    PublishingSpec publishing(Closure configurationClosure) {
        this.publishing = ConfigureUtil.configure(configurationClosure, new PublishingSpec(project))
        return publishing;
    }

    public static class CredentialsSpec {
        /**
         * RooX ID
         */
        String username;

        /**
         * Пароль clear-text'ом, не секьюрно
         */
        String password;

        /**
         * Токен доступа к API. Более секьюрно, чем пароль, но пока не придумано, как он будет работать
         */
        String token;

        CredentialsSpec(Project project) {
            if (project.hasProperty('rooxUser')) {
                username = project.ext.rooxUser;
            }
            if (project.hasProperty('rooxPassword')) {
                password = project.ext.rooxPassword;
            }
        }
    }

    public static class PluginSpec {

        String title

        String summary

        String groupId

        String artifactId

        String version

        boolean platform = false

        boolean featured = false

        boolean official = false

        String author

        String[] tags = []

        ExtSpec ext

        Project project

        PluginSpec(Project project, CredentialsSpec credentialsSpec) {
            this.project = project
            this.title = project.name
            this.summary = project.description
            this.groupId = project.group
            this.artifactId = project.name
            this.version = project.version?.toString()
            this.author = credentialsSpec.username
            this.ext = new ExtSpec(project);
        }

        ExtSpec ext(Closure configurationClosure) {
            this.ext = ConfigureUtil.configure(configurationClosure, new ExtSpec(project))
            return ext;
        }
    }

    public static class ExtSpec {

        def landing
        def sources
        def issues
        def continuousIntegration
        def releaseNotes
        def codeHealth
        def developersGuide
        def apidoc
        def logging
        def healthchecks
        def monitoring
        def configurationProperties

        Project project

        ExtSpec(Project project) {
            this.project = project
            releaseNotes = 'releaseNotes.adoc'
            developersGuide = 'developersGuide.adoc'
            logging = 'logging.csv'
            healthchecks = 'healthchecks.csv'
            monitoring = 'monitoring.csv'
            configurationProperties = 'configurationProperties.csv'
        }
    }

    public static class PublishingSpec {


        String sdkApiUrl = SDKPlugin.DEFAULT_PUBLISHING_URL

        PublishingSpec(Project project) {
            if (project.hasProperty('rooxSdkApiUrl')) {
                sdkApiUrl = project.ext.rooxSdkApiUrl;
            }
        }
    }
}