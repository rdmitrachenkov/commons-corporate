package com.rooxteam.sdk

import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.tasks.TaskAction

/**
 * Uploads content to confluence
 */

class SDKPublishTask extends DefaultTask {


    SDKExtension extension = project.extensions.getByType(SDKExtension);

    String sdkApiUrl = extension?.publishing?.sdkApiUrl;

    String username = extension?.credentials?.username == null;

    String password = extension?.credentials?.password == null;


    @TaskAction
    def publish() {
        if (extension?.credentials?.token == null) {
            logger.warn("Authentication by token is not supported in this version of roox-sdk plugins")
        }
        if (username == null) {
            throw new GradleException("username is not configured in rooxsdk extension or rooxUser property")
        }
        if (password == null) {
            throw new GradleException("password is not configured in rooxsdk extension or rooxPassword property")
        }

        SDKExtension.PluginSpec pluginSpec = extension.plugin;

        if (pluginSpec == null) {
            logger.error("Plugin metadata is not configured in rooxsdk.plugin extension section")
            throw new GradleException("Plugin metadata is not configured in rooxsdk.plugin extension section")
        }
    }

    /**
     * Processes single asciidoctor file represented as source FileTreeElement
     * @param element
     */
    protected void uploadPageInWikiFormat(File sourceFile, String title) {
        //todo: find alternatives for HTTPBuilder, Method and ContentType classes for gradle 2.x
        /*logger.lifecycle("Uploading $sourceFile with title $title");
        logger.debug("Using API located at $apiUrl");
        def http = new HTTPBuilder(apiUrl, JSON);
        logger.debug("Using credentials $username / [MASKED]");
        http.auth.basic(username, password);

        String sourceContent = sourceFile.getText('UTF-8');

        String trimedSourceContent = sourceContent.substring(0, sourceContent.size() >= 70 ? 70 : sourceContent.size());
        logger.debug("Raw content: $trimedSourceContent");

        String storageContent = "";

        if (format == ConfluenceFormat.wiki) {

            // convert wiki markup to storage format
            http.request(POST) { req ->
                uri.path = 'contentbody/convert/storage';
                body = [representation: 'wiki', value: sourceContent];

                response.success = { resp, json ->
                    // response handling here
                    storageContent = json.value;
                }
                response.failure = { resp, json ->
                    logger.error("Failed to convert content $body. " +
                            "Error code: $resp.statusLine.statusCode. " +
                            "Response body: $json");
                    throw new GradleException("Failed to convert document $sourceFile");
                }
            }
            logger.debug("Got converted content: ${storageContent.substring(0, storageContent.size() >= 70 ? 70 : storageContent.size())}");
        } else if (format == ConfluenceFormat.storage) {
            storageContent = sourceContent;
        } else {
            throw new GradleException("Unknown/unsupported format $format");
        }

        // check if page exists and get its version
        boolean exists = false;
        int newVersion = 1;
        String contentId;

        http.request(GET) { req ->
            uri.path = 'content';
            uri.query = [spaceKey: space, title: title, expand: 'version', os_authType: 'basic'];
            response.success = { resp, json ->
                // response handling here
                if (json.size == 1) {
                    exists = true;
                    newVersion = 1 + json.results[0].version.number;
                    contentId = json.results[0].id;
                } else if (json.size > 1) {
                    throw new GradleException("There are $json.size pages with the same title - can't handle this");
                }
            }
            response.failure = { resp, json ->
                logger.error("Failed to check content existance by title $title. " +
                        "Error code: $resp.statusLine.statusCode. " +
                        "Response body: $json");
                throw new GradleException("Failed to check content existance by title $title.");
            }
        }
        if (!exists) {
            http.request(POST) { req ->
                uri.path = 'content';
                uri.query = [os_authType: 'basic'];
                body = [type : "page",
                        title: title,
                        space: [key: space],
                        body : [storage: [value: storageContent, representation: 'storage']]];
                response.success = { resp, json ->
                    // response handling here
                    contentId = json.id;
                }
                response.failure = { resp, json ->
                    logger.error("Failed to upload content " +
                            "Error code: $resp.statusLine.statusCode. " +
                            "Response body: $json");
                    throw new GradleException("Failed to upload content");
                }
            }
        } else {
            http.request(PUT) { req ->
                uri.path = "content/$contentId";
                uri.query = [os_authType: 'basic'];
                body = [id     : contentId,
                        type   : "page",
                        title  : title,
                        space  : [key: space],
                        body   : [storage: [value: storageContent, representation: 'storage']],
                        version: ["number": newVersion]];
                response.success = { resp, json ->
                    // response handling here
                    contentId = json.id;
                }
                response.failure = { resp, json ->
                    logger.error("Failed to upload content " +
                            "Error code: $resp.statusLine.statusCode. " +
                            "Response body: $json");
                    throw new GradleException("Failed to upload content");
                }
            }
        }
        logger.debug("Document '$title' was uploaded to content id: $contentId as version $newVersion");*/
    }

}
