package com.rooxteam.sdk

import org.gradle.api.Project
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.tasks.bundling.Jar
import org.gradle.api.tasks.bundling.Zip

/**
 * Определяет задачи и конфигурацию для java-плагинов
 */
class JavaBasedPlugin extends SDKPlugin {


    @Override
    void applyPlugins(Project project) {
        super.applyPlugins(project)
        // groovy extends java so java plugin apply is not needed
        project.apply(plugin: 'groovy');
    }


    @Override
    void apply(Project project) {
        super.apply(project)
        createProvidedConfiguration(project);
    }

    void createProvidedConfiguration(Project project) {
        // from http://stackoverflow.com/questions/18738888/how-to-use-provided-scope-for-jar-file-in-gradle-build
        project.configurations {
            provided
        }
        project.sourceSets.main.compileClasspath += project.configurations.provided
        project.sourceSets.test.compileClasspath += project.configurations.provided
        project.sourceSets.test.runtimeClasspath += project.configurations.provided

        project.gradle.taskGraph.whenReady {
            if (project.plugins.hasPlugin('idea')) {
                project.idea {
                    module {
                        //if you need to put 'provided' dependencies on the classpath
                        scopes.PROVIDED.plus += project.configurations.provided
                    }
                }
            }

            if (project.plugins.hasPlugin('eclipse')) {
                project.eclipse {
                    classpath {
                        plusConfigurations += [project.configurations.provided]
                    }
                }
            }
        }
    }

    @Override
    void createTasks(Project project) {
        super.createTasks(project)
        if (project.tasks.findByName('docZip') == null) {
            project.task('docZip', type: Zip) {
                archiveName 'doc.zip'
                from 'src/doc'
            }
        }
        project.javadoc {
            classpath = project.sourceSets.main.compileClasspath;
            source(project.sourceSets.main.allJava);
            options {
                encoding = 'UTF-8';
                charSet = 'UTF-8';
            }
        }

        project.task([type: Jar], 'sourceJar') {
            classifier = 'sources';
            from(project.sourceSets.main.allSource);
        }

        project.task([type: Jar, dependsOn: project.javadoc], 'javadocJar') {
            classifier = 'javadoc';
            from(project.javadoc.destinationDir);
        }

        project.publish.dependsOn('javadocJar');
        project.publish.dependsOn('sourceJar');
    }

    @Override
    void createPublishingTasks(Project project) {
        super.createPublishingTasks(project)
        project.publishing {
            publications {
                rooxsdk(MavenPublication) {
                    if (project.tasks.findByName('sourceJar') != null) {
                        // Publish the output of the sourceJar task
                        artifact source: project.sourceJar, classifier: 'sources', extension: 'zip'
                    }
                    if (project.tasks.findByName('javadocJar') != null) {
                        artifact source: project.javadocJar, classifier: 'javadoc', extension: 'zip'
                    }
                    if (project.tasks.findByName('jar') != null) {
                        // Publish the output of the jar task
                        artifact source: project.jar  // Publish the output of the jar task
                    }
                }
            }
        }
    }
}
