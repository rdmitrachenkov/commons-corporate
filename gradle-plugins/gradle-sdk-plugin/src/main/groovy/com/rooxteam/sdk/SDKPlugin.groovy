package com.rooxteam.sdk

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.XmlProvider
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.tasks.bundling.Zip

/**
 * Предоставляет функциональность работы с RooX SDK
 * (публикация плагинов, сетап окружения(?), все остальное)
 * @author kkorsakov
 */
public abstract class SDKPlugin implements Plugin<Project> {

    private static Logger logger = Logging.getLogger(SDKPlugin)

    public static final String DEFAULT_PUBLISHING_URL = "https://sdk.rooxcloud.com/plugins/repositories/releases"

    SDKExtension extension


    @Override
    public void apply(Project project) {
        applyPlugins(project)
        extension = project.extensions.create("rooxsdk", SDKExtension, project)
        createPublishingTasks(project)
        configurePublishingRepo(project)
        createTasks(project)
    }

    /**
     * Применяет плагины, от которых зависит работа текущего плагина
     * @param project
     */
    void applyPlugins(Project project) {
        project.apply(plugin: 'base')
        project.apply(plugin: 'maven-publish')
    }

    /**
     * Создает дополнительные задачи
     * @param project
     */
    void createTasks(Project project) {
        if(project.tasks.findByName('docZip') == null) {
            project.task('docZip', type: Zip) {
                archiveName 'doc.zip'
                from 'src/doc'
            }
        }
    }

    void configurePublishingRepo(Project project) {
        project.publishing {
            repositories {
                maven {
                    name 'rooxsdk'
                    url extension.publishing.sdkApiUrl
                    credentials {
                        username extension?.credentials?.username
                        password extension?.credentials?.password
                    }
                }
            }
        }
    }

    /**
     * Создает задачи на паблишинг
     * @param project
     */
    void createPublishingTasks(Project project) {
        project.publishing {
            publications {
                rooxsdk(MavenPublication) {
                    groupId extension.plugin.groupId
                    artifactId extension.plugin.artifactId
                    version extension.plugin.version

                    pom.withXml(createPom.curry(project))
                    if (project.tasks.findByName('docZip') != null) {
                        artifact source: project.docZip, classifier: 'doc', extension: 'zip'
                    }
                }
            }
        }
    }

    Closure createPom = { Project project, XmlProvider xmlProvider ->
        def root = xmlProvider.asNode()
        root.appendNode('description', extension.plugin?.summary)
        Node propertiesNode = root.properties ?root.properties[0] : root.appendNode('properties')
        propertiesNode.appendNode('rooxsdk.plugin.author', extension.plugin?.author)
        propertiesNode.appendNode('rooxsdk.plugin.featured', extension.plugin?.featured)
        propertiesNode.appendNode('rooxsdk.plugin.official', extension.plugin?.official)
        propertiesNode.appendNode('rooxsdk.plugin.platform', extension.plugin?.platform)
        propertiesNode.appendNode('rooxsdk.plugin.tags', extension.plugin?.tags)
        propertiesNode.appendNode('rooxsdk.plugin.title', extension.plugin?.title)

        propertiesNode.appendNode('rooxsdk.plugin.ext.landing', extContentAsString(project, extension.plugin?.ext?.landing))
        propertiesNode.appendNode('rooxsdk.plugin.ext.sources', extContentAsString(project, extension.plugin?.ext?.sources))
        propertiesNode.appendNode('rooxsdk.plugin.ext.issues', extContentAsString(project, extension.plugin?.ext?.issues))
        propertiesNode.appendNode('rooxsdk.plugin.ext.continuousIntegration', extContentAsString(project, extension.plugin?.ext?.continuousIntegration))
        propertiesNode.appendNode('rooxsdk.plugin.ext.releaseNotes', extContentAsString(project, extension.plugin?.ext?.releaseNotes))
        propertiesNode.appendNode('rooxsdk.plugin.ext.codeHealth', extContentAsString(project, extension.plugin?.ext?.codeHealth))
        propertiesNode.appendNode('rooxsdk.plugin.ext.developersGuide', extContentAsString(project, extension.plugin?.ext?.developersGuide))
        propertiesNode.appendNode('rooxsdk.plugin.ext.apidoc', extContentAsString(project, extension.plugin?.ext?.apidoc))
        propertiesNode.appendNode('rooxsdk.plugin.ext.logging', extContentAsString(project, extension.plugin?.ext?.logging))
        propertiesNode.appendNode('rooxsdk.plugin.ext.healthchecks', extContentAsString(project, extension.plugin?.ext?.healthchecks))
        propertiesNode.appendNode('rooxsdk.plugin.ext.monitoring', extContentAsString(project, extension.plugin?.ext?.monitoring))
        propertiesNode.appendNode('rooxsdk.plugin.ext.configurationProperties', extContentAsString(project, extension.plugin?.ext?.configurationProperties))
    }

    /**
     * Преобразует свободный формат указания информации в блоке ext в формат пригодный для описания в дескрипторе плагина.
     * Принимает:
     * <ul>
     *     <li>File - содержимое считывается и вставляется целиком
     *     <li>URL - если HTTP(S) - возвращается как есть, иначе возвращается null
     *     <li>String/GString - производится попытка поиска см ниже
     * </ul>
     * <ul>
     *     <li>
     * @param extContent
     * @param project
     * @return
     */
    String extContentAsString(Project project, extContent) {
        if (extContent == null) return null

        URL url = null
        try {
            url = extContent.toString().toURL()
        } catch (e) {
            project.logger.debug("$extContent is not an URL")
        }
        if (url != null) {
            String protocol = url.getProtocol()
            if (protocol != null && (protocol == 'http' || protocol == 'https')) {
                // это разрешенный внешний URL - отдаем как есть
                return url.toString()
            }
        }

        def file;
        try {
            file = project.file('src/doc/' + extContent)
        } catch (e) {
            logger.debug("$extContent is not a file inside src/doc")
        }
        if (file != null && file.isFile()) {
            // это файл внутри документации - отдаем путь как есть с расчетом на то, что сервер сам соберет доку и проставит полный путь
            return extContent
        }
        try {
            file = project.file(extContent)
        } catch (e) {
            logger.debug("$extContent is not a file inside project directory")
        }
        if (file != null && project.file(extContent).isFile()) {
            // это файл внутри проекта - отдаем его содержимое
            return project.file(extContent).getText("UTF-8")
        }

        // непонятно что, вероятно просто текстом - отдаем как есть
        extContent
    }
}

