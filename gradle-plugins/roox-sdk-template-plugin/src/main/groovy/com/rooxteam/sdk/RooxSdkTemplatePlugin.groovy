package com.rooxteam.sdk

import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 */
class RooxSdkTemplatePlugin implements Plugin<Project> {

    @Override
    void apply(Project target) {
        configureProject(target)
    }

    void configureProject(Project rootProject) {
        rootProject.allprojects { project ->
            project.apply(plugin: "com.orctom.archetype");
            addTasks(project);
        }
    }

    void addTasks(Project project) {
        addTemplateTask(project, "webapi-boot-j7");
    }

    void addTemplateTask(Project project, String templateName) {
        project.tasks.create("generate-" + templateName, GenerateTemplateProjectTask) { task->
            task.template = templateName;
        }
    }
}
