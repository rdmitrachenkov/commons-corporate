package com.rooxteam.sdk;

import com.orctom.gradle.archetype.ArchetypeGenerateTask;
import org.gradle.api.GradleException;
import org.gradle.api.tasks.TaskAction;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 *
 */
public class GenerateTemplateProjectTask extends ArchetypeGenerateTask {

    String template;


    public GenerateTemplateProjectTask() {
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    @TaskAction
    public void run() throws IOException {
        if (template == null || template.length() == 0) {
            throw new GradleException("template cannot be empty");
        }
        String dest = unpackTemplate(template);
        System.setProperty("templates", dest);
        super.create();
    }

    private String unpackTemplate(String template) throws IOException {
        String resourcesZipPath = "META-INF/templates.zip";
        URL resourcesZip = GenerateTemplateProjectTask.class.getResource(resourcesZipPath);
        if (resourcesZip == null) {
            throw new GradleException("could not find template");
        }
        // the file is a jar file - write it to disk first
        InputStream zipInputStream = (InputStream) resourcesZip.getContent();
        File zipFile = new File(getProject().getBuildDir() + "/templates.zip");
        File templates = new File(getProject().getBuildDir() + "/templates");
        copyFile(zipInputStream, zipFile);
        getProject().copy(copySpec -> {
            copySpec.from(getProject().zipTree(zipFile)).into(templates);
        });
        File templateDir = new File(getProject().getBuildDir(), template);
        if (!templateDir.exists()) {
            throw new GradleException("template doesn't exist");
        }
        return "build" + File.separator + template;
    }

    private void copyFile(InputStream source, File destFile) throws IOException {
        destFile.createNewFile();
        FileOutputStream to = null;
        try {
            to = new FileOutputStream(destFile);
            byte[] buffer = new byte[4096];
            int bytesRead;

            while ((bytesRead = source.read(buffer)) > 0) {
                to.write(buffer, 0, bytesRead);
            }
        } finally {
            if (source != null) {
                source.close();
            }
            if (to != null) {
                to.close();
            }
        }
    }
}
